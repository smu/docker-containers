#!/bin/bash
set -eu

cd $(dirname $0)
docker build -t cabal:1.24.0.2 --no-cache .
