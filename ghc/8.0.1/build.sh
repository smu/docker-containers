#!/bin/bash
set -eu

cd $(dirname $0)
docker build -t ghc:8.0.1 --no-cache .
