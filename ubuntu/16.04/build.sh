#!/bin/bash
set -eu

cd $(dirname $0)
docker build -t ubuntu:16.04 --no-cache .
